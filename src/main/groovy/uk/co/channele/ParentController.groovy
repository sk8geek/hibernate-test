package uk.co.channele

import io.micronaut.http.HttpResponse
import io.micronaut.http.annotation.Body
import io.micronaut.http.annotation.Controller
import io.micronaut.http.annotation.Delete
import io.micronaut.http.annotation.Get
import io.micronaut.http.annotation.PathVariable
import io.micronaut.http.annotation.Post
import io.micronaut.http.annotation.Put

@Controller
class ParentController {

    private final ParentService service

    ParentController(ParentService service) {
        this.service = service
    }

    @Get("/{id}")
    def getParent(@PathVariable Long id) {
        def response = service.get(id)
        if (response) {
            return HttpResponse.ok(response)
        }
        return HttpResponse.notFound()
    }

    @Post()
    def postParent(@Body ParentRequest request) {
        def response = service.create(request)
        if (response) {
            return HttpResponse.created(response)
        }
        return HttpResponse.badRequest()
    }

    @Put("/{id}")
    def updateParent(@PathVariable Long id, @Body ParentRequest request) {
        def response = service.update(id, request)
        if (response) {
            return HttpResponse.created(response)
        }
        return HttpResponse.badRequest()
    }

    @Delete("/{id}")
    def deleteParent(@PathVariable Long id) {
        def response = service.delete(id)
        if (response) {
            return HttpResponse.noContent()
        }
        return HttpResponse.badRequest()
    }


}
