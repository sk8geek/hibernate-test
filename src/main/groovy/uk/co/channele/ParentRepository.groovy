package uk.co.channele

import io.micronaut.data.annotation.Repository
import io.micronaut.data.jpa.repository.JpaRepository

@Repository
interface ParentRepository extends JpaRepository<Parent, Long> {


}
