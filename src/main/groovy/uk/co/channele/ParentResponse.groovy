package uk.co.channele

class ParentResponse extends CommonResponse {

    ParentResponse() {}

    ParentResponse(String name) {
        this.name = name
    }

    ParentResponse(Long id, String name) {
        this.id = id
        this.name = name
    }

    String name

    HashSet<ChildResponse> children = new HashSet<>()

}
