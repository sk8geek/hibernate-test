package uk.co.channele

import javax.inject.Singleton

@Singleton
class ParentService {

    private final ParentRepository repo

    ParentService(ParentRepository repo) {
        this.repo = repo
    }


    ParentResponse get(Long id) {
        def optParent = repo.findById(id)
        if (optParent.isPresent()) return toResponse(optParent.get())
        return null
    }


    ParentResponse create(ParentRequest request) {
        def parent = fromRequest(request)
        parent = repo.save(parent)
        return toResponse(parent)
    }


    ParentResponse update(Long id, ParentRequest request) {
        def parent = repo.findById(id)
        if (parent.isEmpty()) return null
        return toResponse(repo.update(updateFromRequest(parent.get(), request)))
    }


    boolean delete(Long id) {
        repo.deleteById(id)
        return true
    }


    private static ParentResponse toResponse(Parent parent) {
        def response = new ParentResponse(parent.id, parent.name)
        parent.children?.each { response.children.add(new ChildResponse(it.id, it.name)) }
        return response
    }

    private static Parent fromRequest(ParentRequest request) {
        def parent = new Parent(request.name)
        request.children?.each {parent.children.add(new Child(it.name))}
        return parent
    }

    private static Parent updateFromRequest(Parent parent, ParentRequest request) {
        if (request.name) parent.name = request.name
        parent.children.clear()
        request.children?.each {parent.children.add(new Child(it.name)) }
        return parent
    }

}
