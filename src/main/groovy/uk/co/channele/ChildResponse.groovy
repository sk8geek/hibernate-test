package uk.co.channele

class ChildResponse extends CommonResponse {

    ChildResponse() {}

    ChildResponse(String name) {
        this.name = name
    }

    ChildResponse(Long id, String name) {
        this.id = id
        this.name = name
    }

    String name

}
