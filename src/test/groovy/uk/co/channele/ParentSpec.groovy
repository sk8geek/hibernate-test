package uk.co.channele

import io.micronaut.http.HttpRequest
import io.micronaut.http.HttpStatus
import io.micronaut.http.client.HttpClient
import io.micronaut.http.client.annotation.Client
import io.micronaut.test.extensions.spock.annotation.MicronautTest
import spock.lang.Specification

import javax.inject.Inject

@MicronautTest
class ParentSpec extends Specification {

    @Inject
    @Client("/")
    HttpClient client


    void "test Hibernate cascade"() {

        when: "create a parent"
        def parentRequest = new ParentRequest("Alice")
        parentRequest.children.add(new ChildRequest("Bob"))
        parentRequest.children.add(new ChildRequest("Claire"))
        parentRequest.children.add(new ChildRequest("Dave"))

        def request = HttpRequest.POST("/", parentRequest)
        def response = client.toBlocking().retrieve(request, ParentResponse)

        def id = response.getId()

        then: "parent is created with three children"
        id
        response.children.size() == 3
        response.children[0].id
        response.children[1].id
        response.children[2].id

        when: "update"
        Set<Long> childIds = response.children.collect { it.id }
        def updateRequest = new ParentRequest("Alice")
        updateRequest.children.add(new ChildRequest("Elizabeth"))

        def requestUpdate = HttpRequest.PUT("/${id}", updateRequest)
        def responseUpdate = client.toBlocking().retrieve(requestUpdate, ParentResponse)

        then: "parent now only has one child"
        responseUpdate.children.size() == 1
        childIds.disjoint([responseUpdate.children[0].id])

        when: "test delete"
        def requestDelete = HttpRequest.DELETE("/${id}")
        def responseDelete = client.toBlocking().exchange(requestDelete)

        then: "no content response"
        responseDelete.status == HttpStatus.NO_CONTENT

    }


}
